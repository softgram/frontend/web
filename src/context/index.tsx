'use client';

import { TanStackQueryProvider } from './tanstack-query';
import { ThemeProvider, createTheme } from '@mui/material';

type RootProvidersProps = {
  children: React.ReactNode;
};

export const RootProviders = ({
  children,
}: RootProvidersProps): React.ReactElement => {
  const theme = createTheme({
    palette: {
      primary: {
        main: '#222',
      },
    },
  });

  return (
    <TanStackQueryProvider>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </TanStackQueryProvider>
  );
};
