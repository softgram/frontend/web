'use client';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

type TanStackQueryProps = {
  children: React.ReactNode;
};

export const TanStackQueryProvider = ({
  children,
}: TanStackQueryProps): React.ReactElement => {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
  );
};
