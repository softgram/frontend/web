import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import '../globals.css';
import { RootProviders } from '@context';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Softgram',
  description: 'Softgram',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="es">
      <body className={inter.className}>
        <RootProviders>{children}</RootProviders>
      </body>
    </html>
  );
}
