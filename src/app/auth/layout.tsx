type AuthLayoutProps = {
  children: React.ReactNode;
};

const AuthLayout = ({ children }: AuthLayoutProps): React.ReactElement => {
  return (
    <section className="w-screen h-screen grid grid-cols-9 ">
      <div className="col-span-4"></div>
      {children}
    </section>
  );
};

export default AuthLayout;
